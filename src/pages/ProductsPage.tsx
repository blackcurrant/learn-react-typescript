import React, { useContext } from "react"

import { useProducts } from "../hooks/products"
import { ModalContext } from "../context/ModalContext"
import { IProduct } from "../models"

import { Loader } from "../components/Loader"
import { ErrorMessage } from "../components/ErrorMessage"
import { Product } from "../components/Product"
import { Modal } from "../components/Modal"
import { CreateProduct } from "../components/CreateProduct"

export const ProductsPage = () => {
    const { products, isLoading, errorMessage , addProduct } = useProducts()
    const { isOpenModal, openModal, closeModal } = useContext(ModalContext)


    const createHandler = (product: IProduct) => {
        closeModal()
        addProduct(product)
    }

    return (
        <div className="container mx-auto max-w-2xl pt-5">
            { isLoading && <Loader /> }
            { errorMessage && <ErrorMessage text={ errorMessage } /> }
            { products.map(p => <Product key={ p.id } product={ p }/>) }

            { isOpenModal && (
                <Modal title="New product" onClose={ () => closeModal() }>
                    <CreateProduct onCreate={ createHandler }/>
                </Modal>
            ) }

            <button
                className="fixed bottom-5 right-5 rounded-full bg-red-700 text-white text-2xl px-4 py-2"
                onClick={ () => openModal() }
            >+</button>
        </div>
    )
}
