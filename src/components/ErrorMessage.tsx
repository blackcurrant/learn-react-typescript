import React from "react"

interface IErrorMessageProps {
    text: string
}

export const ErrorMessage = ({ text }: IErrorMessageProps) => {
    return <p className="text-center text-red-600">{ text }</p>
}
