import React, { useState } from "react"
import axios from "axios"

import { IProduct } from "../models"
import { ErrorMessage } from "./ErrorMessage"

const productData: IProduct = {
    title: 'test product',
    price: 13.5,
    description: 'lorem ipsum set',
    image: 'https://i.pravatar.cc',
    category: 'electronic',
    rating: {
        rate: 42,
        count: 10
    }
}

interface ICreateProductProps {
    onCreate: (product: IProduct) => void
}

export const CreateProduct = ({ onCreate }: ICreateProductProps) => {
    const [value, setValue] = useState("")
    const [errorMessage, setErrorMessage] = useState("")

    const submitHandler = async (event: React.FormEvent) => {
        event.preventDefault()
        setErrorMessage("")

        if (value.trim().length == 0) {
            setErrorMessage("Please enter valid title.")
            return
        }

        productData.title = value
        const response = await axios.post<IProduct>(
            "https://fakestoreapi.com/products", productData,
        )
        onCreate(response.data)
    }

    const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value)
    }

    return (
        <form onSubmit={ submitHandler }>
            <input
                type="text"
                className="border py-2 px-4 mb-2 w-full outline-0"
                placeholder="Enter product title..."
                value={ value }
                onChange={ changeHandler }
            />
            { errorMessage && <ErrorMessage text={ errorMessage } /> }
            <button
                className="border py-2 px-4 bg-yellow-400 w-full"
                type="submit"
            >Create</button>
        </form>
    )
}
