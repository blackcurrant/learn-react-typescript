import React, {useState} from 'react'

import { IProduct } from "../models"

interface IProductProps {
    product: IProduct
}

export const Product = ({ product }: IProductProps) => {
    const [details, setDetails] = useState(false)

    return (
        <div
            className="border py-2 px-4 rounded flex flex-col items-center mb-2"
        >
            <img
                src={ product.image }
                className="w-1/6"
                alt={ product.title }
            />
            <p>{ product.title }</p>
            <p className="font-bold">{ product.price }</p>
            <button
                className={`py-2 px-4 border bg-${ details ? 'red' : 'yellow' }-400 min-w-[20%]`}
                onClick={() => setDetails(prev => !prev)}
            >{ details ? 'Hide' : 'Show' } details
            </button>

            { details && (
                <div>
                    <p>{ product.description }</p>
                    <p>Rate: <span className="font-bold">{ product?.rating?.rate }</span></p>
                    <p>Count: <span className="font-bold">{ product?.rating?.count }</span></p>
                </div>
            ) }
        </div>
    )
}
