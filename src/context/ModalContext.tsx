import React, { createContext, useState } from "react"

interface IModalContext {
    isOpenModal: boolean
    openModal: () => void
    closeModal: () => void
}

export const ModalContext = createContext<IModalContext>({
    isOpenModal: false,
    openModal: () => {},
    closeModal: () => {}
})

export const ModalState = ({ children }: { children: React.ReactNode }) => {
    const [isOpenModal, setIsOpenModal] = useState(false)

    const openModal = () => setIsOpenModal(true)
    const closeModal = () => setIsOpenModal(false)

    return (
        <ModalContext.Provider value={{ isOpenModal, openModal, closeModal }}>
            { children }
        </ModalContext.Provider>
    )
}
