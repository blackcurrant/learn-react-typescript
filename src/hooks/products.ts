import { useEffect, useState } from "react"
import axios, { AxiosError } from "axios"

import { IProduct } from "../models"

export const useProducts = () => {
    const [products, setProducts] = useState<IProduct[]>([])
    const [isLoading, setIsLoading] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    const addProduct = (product: IProduct) => {
        setProducts(prev => [...prev, product])
    }

    const fetchProducts = async () => {
        try {
            setErrorMessage("")
            setIsLoading(true)
            const response = await axios.get<IProduct[]>("https://fakestoreapi.com/products?limit=5")
            setProducts(response.data)
        } catch (e: unknown) {
            const error = e as AxiosError
            setErrorMessage(error.message)
        } finally {
            setIsLoading(false)
        }
    }

    useEffect(() => {
        fetchProducts()
    }, []);

    return { products, isLoading, errorMessage, addProduct }
}